# Color Game Project

Simple game where player have to pick what color is show as RGB number.

The program generates random colors and set it up as div's backgrounds. One of color is also shown on the top as RGB color.
When user click on square, program grabs background color of it and compare to color on the top.
If color match, user see message "Correct!" and then game is end, when color doesn't match, message "Try again" is shown.
Game got also couple of other functions like two difficulty levels or new colors.

The game was made as a part of Colt Steele's "The Web Developer Bootcamp" course on Udemy to learn JS basics.
## Installation

Just open index.html on browser.


## Tech

HTML5, CSS3 and Vanilla JS

## Screenshot

![color-game-js](https://bitbucket.org/kodukodu/color-game-js/raw/942891a3376f8bccf4f5509e741dd13f794eec62/img/color-game-js.png)